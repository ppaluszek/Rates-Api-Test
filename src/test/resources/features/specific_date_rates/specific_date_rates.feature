Feature: Foreign exchange rate for specific date
  In order to get foreign exchange rate for specific date
  As a user with access to Rates API
  I want to receive historical exchange rate for specific date since 1999

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @Acceptance @SpecificDate @Regression
  Scenario: Martin wants to receive rates for date from future
    When Martin sends a GET request for "2050-01-15" endpoint
    Then he receives response with status 200
    And he receives the date of latest available exchange rates

  @Acceptance @SpecificDate @Regression
  Scenario: Martin wants to receive rates for specific date
    When Martin sends a GET request for "2015-01-17" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "EUR"
    And he receives date one day before specific date ["2015-01-16"] in response
    And he should see list of currencies with rates:
      | GBP | 0.7637   |
      | HKD | 8.983    |
      | IDR | 14581.91 |
      | ILS | 4.542    |
      | DKK | 7.4346   |
      | INR | 71.6857  |
      | CHF | 1.0128   |
      | MXN | 16.9787  |
      | CZK | 27.795   |
      | SGD | 1.5363   |
      | THB | 37.798   |
      | HRK | 7.693    |
      | MYR | 4.1234   |
      | NOK | 8.7985   |
      | CNY | 7.1926   |
      | BGN | 1.9558   |
      | PHP | 51.684   |
      | SEK | 9.4085   |
      | PLN | 4.3179   |
      | ZAR | 13.403   |
      | CAD | 1.3946   |
      | BRL | 3.0357   |
      | RON | 4.5083   |
      | NZD | 1.4871   |
      | TRY | 2.6888   |
      | JPY | 135.06   |
      | RUB | 75.658   |
      | KRW | 1248.7   |
      | USD | 1.1588   |
      | HUF | 320.37   |
      | AUD | 1.4113   |
