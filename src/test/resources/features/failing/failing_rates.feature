Feature: Demonstrate failing tests
  In order to demonstrate failing test
  As a User with access to Rates API
  I want to see failing tests

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @Failing
  Scenario: Martin wants to receive only one rate for specific date with two the same symbols
    And parameter "symbols" is "USD,USD"
    When Martin sends a GET request for "2017-05-05" endpoint
    Then he receives date "2017-05-05" in response
    And he should see list of currencies with rates:
      | USD | 1.0 |


  @Failing
  Scenario: Martin wants to receive rate for specific date with specific symbols
    Given parameter "symbols" is "USD"
    When Martin sends a GET request for "2018-05-03" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "USD"
    And he receives date "2018-05-03" in response
    And he should see list of currencies with rates:
      | USD | 1.1992 |
