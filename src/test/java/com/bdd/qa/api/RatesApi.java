package com.bdd.qa.api;

import com.bdd.qa.api.model.ExchangeRates;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;

import static io.restassured.RestAssured.given;

@Getter
public class RatesApi {

    private ExchangeRates exchangeRates;
    private final RequestSpecification request;

    public RatesApi(String apiUrl) {
        RestAssured.baseURI = apiUrl;
        request = given().contentType(ContentType.JSON);
    }

    public ExchangeRates sendGetRequest(String endpoint) {
        Response response = request.get(endpoint);
        mapResponseToObject(response);
        return exchangeRates;
    }

    public void queryParam(String paramType, String paramValue){
        request.queryParam(paramType, paramValue);
    }

    private void mapResponseToObject(Response response) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            exchangeRates = mapper.readValue(response.asString(), ExchangeRates.class);
            exchangeRates.setStatus(response.statusCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
