# Rates API Test with Cucumber

[Rates API](https://ratesapi.io/documentation/) is a free service for current and historical foreign exchange rates built on top of data published by European Central Bank

Automation testing framework which covers different
scenarios for the Rates API to make sure the API’s are fit for purpose in the use of the exchange rate for financial
reasons 

## Technologies
 * Java 8
 * Maven
 * Cucumber JVM 6.8.1
 * JUnit 4
 * Rest Assured 4.3.3
 * Jackson 2.11
 * Lombok 1.18

## Installation

Clone this repository

```bash
$ git clone https://gitlab.com/ppaluszek/Rates-Api-Test.git
```

## Usage

Once you have cloned the repository, to run whole test suite (including failing tests) follow commands:

```bash
$ cd Rates-Api-Test
$ mvn clean test
```
Once all tests completed you will find a link in a console to open report.
Example:

```bash
View your Cucumber Report at:
https://reports.cucumber.io/reports/06c3cf17-fc85-4443-b72a-26ba67644c3d
```

## Running a subset of scenarios

There are several test tags available: 
* @Acceptance - run all acceptance tests
* @Regression - run all regression tests
* @ApiErrors - run only test scenarios with API errors
* @Latest - run only scenarios for latest endpoint
* @SpecificDate - run only scenarios for specific date endpoint
* @Failing - run only scenarios with failing tests

To run tag or tags execute command:

```bash
$ mvn clean test -Dcucumber.filter.tags="@ApiErrors"
```
Multiple tags:

```bash
$ mvn clean test -Dcucumber.filter.tags="@ApiErrors or @Latest"
```

Ignore subset scenarios

```bash
$ mvn clean test -Dcucumber.filter.tags="not @Failing"
```

More information you can find in [Cucumber Documentation](https://cucumber.io/docs/cucumber/api/#running-a-subset-of-scenarios)

## Contact
Created by [P.Paluszek](https://pl.linkedin.com/in/piotr-paluszek-552960a5) - feel free to contact me!


