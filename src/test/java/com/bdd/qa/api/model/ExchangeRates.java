package com.bdd.qa.api.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ExchangeRates {
    private String base;
    private Map<String,Double> rates;
    private String date;
    private String error;
    private int status;

    public List<String> getRatesList(){
        return new ArrayList<>(rates.keySet());
    }

    @Override
    public String toString() {
        return "ExchangeRateResponse{" +
                "base='" + base + '\'' +
                ", rates=" + rates +
                ", date='" + date + '\'' +
                ", error='" + error + '\'' +
                ", status=" + status +
                '}';
    }
}
