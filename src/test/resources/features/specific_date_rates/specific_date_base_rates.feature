Feature: Foreign exchange rates for specific date with non default base currency
  In order to get foreign exchange rates for specific date with different base than EUR
  As a user with access to Rates API
  I want to receive rates for specific date with specific base currency

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @SpecificDate @Regression
  Scenario: Martin want to receive default base for empty base for specific date
    Given parameter "base" is ""
    When Martin sends a GET request for "2019-01-20" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "EUR"

  @SpecificDate @Regression
  Scenario: Martin wants to receive rates for specific date with specific base currency
    Given parameter "base" is "IDR"
    When Martin sends a GET request for "2018-05-13" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "IDR"
    And he should see list of currencies with rates:
      | GBP | 0.0000527809 |
      | HKD | 0.0005618829 |
      | IDR | 1.0          |
      | ILS | 0.0002552677 |
      | DKK | 0.0004468265 |
      | INR | 0.0048198581 |
      | CHF | 0.0000715721 |
      | MXN | 0.0013782779 |
      | CZK | 0.0015298071 |
      | SGD | 0.0000954675 |
      | THB | 0.0022808557 |
      | HRK | 0.0004431978 |
      | EUR | 0.0000599783 |
      | MYR | 0.0002861746 |
      | NOK | 0.0005723432 |
      | CNY | 0.0004532682 |
      | BGN | 0.0001173056 |
      | PHP | 0.0037517041 |
      | PLN | 0.0002554117 |
      | ZAR | 0.0008787724 |
      | CAD | 0.0000911491 |
      | ISK | 0.0073413468 |
      | BRL | 0.0002548239 |
      | RON | 0.0002779695 |
      | NZD | 0.0001026229 |
      | TRY | 0.0003060394 |
      | JPY | 0.0078235726 |
      | RUB | 0.0044126293 |
      | KRW | 0.0763566047 |
      | USD | 0.0000715781 |
      | AUD | 0.0000947717 |
      | HUF | 0.0188913727 |
      | SEK | 0.0006155875 |
