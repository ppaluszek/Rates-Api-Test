Feature: Foreign exchange rates for specific symbols and base currency
  In order to get foreign exchange rate for specific base currency only for specific symbols
  As a user with access to Rates API
  I want to receive latest exchange rates for specific symbols and base in API response

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @Latest @Regression
  Scenario: Martin wants to receive rate for specific base for one symbol only
    Given parameter "base" is "GBP"
    And parameter "symbols" is "USD"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "GBP"
    And he should see list of currencies:
      | USD |

  @Latest @Regression
  Scenario: Martin wants to receive rates for specific base for more than one symbols
    Given parameter "base" is "CHF"
    And parameter "symbols" is "USD,GBP,IDR"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "CHF"
    And he should see list of currencies:
      | USD |
      | IDR |
      | GBP |

  @Latest @Regression
  Scenario: Martin wants to receive rates 1.0 for the same base and symbols
    Given parameter "base" is "USD"
    And parameter "symbols" is "USD"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "USD"
    And he should see list of currencies with rates:
      | USD | 1.0 |

