Feature: Foreign exchange rates for specific symbols
  In order to get foreign exchange rate only for specific symbols
  As a user with access to Rates API
  I want to receive latest exchange rates for specific symbols in API response

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @Latest @Regression
  Scenario: Martin wants to receive rate only for specific symbols
    Given parameter "symbols" is "USD"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "EUR"
    And he should see list of currencies:
      | USD |

  @Latest @Regression
  Scenario: Martin wants to receive rate for more than one specific symbols
    Given parameter "symbols" is "USD,GBP"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "EUR"
    And he should see list of currencies:
      | USD |
      | GBP |

  @Latest @Regression
  Scenario: Martin wants to receive only one rate for two the same symbols
    And parameter "symbols" is "USD,USD"
    When Martin sends a GET request for "latest" endpoint
    Then he should see list of currencies:
      | USD |
