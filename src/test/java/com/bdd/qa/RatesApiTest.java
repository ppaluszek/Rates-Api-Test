package com.bdd.qa;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:/features",
        glue = "com.bdd.qa.steps"
)
public class RatesApiTest {}
