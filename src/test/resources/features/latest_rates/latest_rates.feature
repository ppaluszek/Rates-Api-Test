Feature: Latest foreign exchange rates
  In order to get latest foreign exchange rates
  As a user with access to Rates API
  I want to receive latest exchange rates in API response

  @Acceptance @Latest @Regression
  Scenario: Martin wants to receive latest rates
    Given Rates api url is "https://api.ratesapi.io/api/"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "EUR"
    And he should see list of currencies:
      | GBP |
      | HKD |
      | IDR |
      | ILS |
      | DKK |
      | INR |
      | CHF |
      | MXN |
      | CZK |
      | SGD |
      | THB |
      | HRK |
      | MYR |
      | NOK |
      | CNY |
      | BGN |
      | PHP |
      | SEK |
      | PLN |
      | ZAR |
      | CAD |
      | ISK |
      | BRL |
      | RON |
      | NZD |
      | TRY |
      | JPY |
      | RUB |
      | KRW |
      | USD |
      | HUF |
      | AUD |







