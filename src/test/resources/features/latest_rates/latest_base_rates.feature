Feature: Foreign exchange rates with non default base currency
  In order to get foreign exchange rates for different base than default EUR
  As a user with access to Rates API
  I want to receive latest rates with specific base currency

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @Latest @Regression
  Scenario: Martin wants to receive default base for empty base
    Given parameter "base" is ""
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "EUR"

  @Latest @Regression
  Scenario: Martin wants to receive latest rates with specific base currency
    Given parameter "base" is "USD"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "USD"
    And he should see list of currencies:
      | GBP |
      | HKD |
      | IDR |
      | ILS |
      | DKK |
      | INR |
      | CHF |
      | MXN |
      | CZK |
      | SGD |
      | THB |
      | HRK |
      | EUR |
      | MYR |
      | NOK |
      | CNY |
      | BGN |
      | PHP |
      | PLN |
      | ZAR |
      | CAD |
      | ISK |
      | BRL |
      | RON |
      | NZD |
      | TRY |
      | JPY |
      | RUB |
      | KRW |
      | USD |
      | AUD |
      | HUF |
      | SEK |
