package com.bdd.qa.steps;

import com.bdd.qa.api.RatesApi;
import com.bdd.qa.api.model.ExchangeRates;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;
import java.util.Map;


public class ExchangeRatesSteps {

    private RatesApi api;
    private ExchangeRates response;

    @Given("Rates api url is {string}")
    public void ratesApiUrlIs(String apiUrl) {
        api = new RatesApi(apiUrl);
    }

    @Given("parameter {string} is {string}")
    public void queryParameterTypeAndValueIs(String paramType, String paramValue) {
        api.queryParam(paramType, paramValue);
    }

    @When("Martin sends a GET request for {string} endpoint")
    public void sendingGetRequestForEndpoint(String endpoint) {
        response = api.sendGetRequest(endpoint);
    }

    @Then("he receives response with status {int}")
    public void shouldReceiveResponseStatus(int statusCode) {
        Assert.assertEquals("Response status code does not match!", statusCode, response.getStatus());
    }

    @Then("he receives base rate in response as {string}")
    public void shouldReceiveBaseRateInResponse(String baseRate) {
        Assert.assertEquals("Base exchange rate does not match!", baseRate, response.getBase());
    }

    @Then("he should see list of currencies with rates:")
    public void shouldSeeListOfCurrenciesWithRates(Map<String, Double> expectedRatesList) {
        Assert.assertEquals("Expected list does not equal received list!", expectedRatesList, response.getRates());
    }

    @Then("he receives the date of latest available exchange rates")
    public void shouldReceiveDateOfLatestExchangeRates() {
        String receivedDate = response.getDate();
        api.sendGetRequest("latest");
        String latest = response.getDate();
        Assert.assertEquals("Received date does not equals latest exchange rate date!", receivedDate, latest);
    }

    @And("he see error message {string}")
    public void shouldSeeErrorMessage(String errorMsg) {
        Assert.assertEquals("Error message does not match!", errorMsg, response.getError());
    }


    @And("he see error message like {string}")
    public void shouldSeeErrorMessageLike(String errorMsg) {
        Assert.assertTrue("Expected error msg does not contains in received error msg!", response.getError().contains(errorMsg));
    }

    @And("he should see list of currencies:")
    public void shouldSeeListOfCurrencies(List<String> expectedCurrencies) {
        Assert.assertEquals("Expected list does not equal received list!", expectedCurrencies, response.getRatesList());
    }

    @And("he receives date one day before specific date [{string}] in response")
    public void shouldReceiveDateOneDayBeforeSpecificDate(String expectedDate) {
        Assert.assertEquals("Expected date does not equal received exchange rate date!", expectedDate, response.getDate());
    }

    @And("he receives date {string} in response")
    public void shouldReceiveDate(String expectedDate) {
        Assert.assertEquals("Expected date does not equal received date!",expectedDate, response.getDate());
    }
}
