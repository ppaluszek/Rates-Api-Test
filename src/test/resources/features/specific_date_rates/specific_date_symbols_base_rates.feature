Feature: Foreign exchange rate for specific date with specific symbols and base currency
  In order to get foreign exchange rate for specific date with specific base currency and symbols
  As a user with access to Rates API
  I wan t to receive exchange rates for specific dates with specific symbols and base currency

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @SpecificDate @Regression
  Scenario: Martin wants to receive rate for specific date with specific base currency and one symbol only
    Given parameter "base" is "GBP"
    And parameter "symbols" is "USD"
    When Martin sends a GET request for "2018-05-09" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "GBP"
    And he receives date "2018-05-09" in response
    And he should see list of currencies with rates:
      | USD | 1.3584538853 |

  @SpecificDate @Regression
  Scenario: Martin wants to receive rates for specific date with specific base for more than one symbols
    Given parameter "base" is "CHF"
    And parameter "symbols" is "USD,GBP,IDR"
    When Martin sends a GET request for "2018-12-05" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "CHF"
    And he receives date "2018-12-05" in response
    And he should see list of currencies with rates:
      | USD | 1.0022951977     |
      | IDR | 14428.0367231638 |
      | GBP | 0.7846486582     |

  @SpecificDate @Regression
  Scenario: Martin wants to receive rates 1.0 for specific date with the same base and symbols
    Given parameter "base" is "USD"
    And parameter "symbols" is "USD"
    When Martin sends a GET request for "2011-07-08" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "USD"
    And he receives date "2011-07-08" in response
    And he should see list of currencies with rates:
      | USD | 1.0 |
