Feature: API errors
  In order to get api errors
  As a user with access to Rates PI
  I want to receive api errors in API response

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @Acceptance @ApiErrors @Regression
  Scenario: Martin wants to see error for request sent to non existing endpoint
    When Martin sends a GET request for "nonexisting" endpoint
    Then he receives response with status 400
    And he see error message "time data 'nonexisting' does not match format '%Y-%m-%d'"

  @ApiErrors @Regression
  Scenario: Martin wants to see error for request with invalid date format
    When Martin sends a GET request for "2011-01-011" endpoint
    Then he receives response with status 400
    And he see error message "unconverted data remains: 1"

  @ApiErrors @Regression
  Scenario: Martin wants to see error for request with date before 1999
    When Martin sends a GET request for "1998-05-05" endpoint
    Then he receives response with status 400
    And he see error message "There is no data for dates older then 1999-01-04."

  @ApiErrors @Regression
  Scenario Outline: Martin wants to see error for request with invalid symbols
    Given parameter "symbols" is "<Symbol>"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 400
    And he see error message like "Symbols '<Symbol>' are invalid"

    Examples:
      | Symbol   |
      | NON      |
      | USD,EUR, |

  @ApiErrors @Regression
  Scenario Outline: Martin wants to see an error for request for invalid base currency
    Given parameter "base" is "<Base>"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 400
    And he see error message "Base '<Base>' is not supported."

    Examples:
      | Base    |
      | USD1    |
      | USD,USD |

  @ApiErrors @Regression
  Scenario Outline: Martin wants to see an error for request for invalid base/symbols
    Given parameter "base" is "<Base>"
    And parameter "symbols" is "<Symbol>"
    When Martin sends a GET request for "latest" endpoint
    Then he receives response with status 400
    And he see error message like "<Error>"

    Examples:
      | Symbol   | Base | Error                          |
      | USD,GBP  | EURR | Base 'EURR' is not supported.  |
      | USD,EURR | GBP  | Symbols 'USD,EURR' are invalid |

