Feature: Foreign exchange for specific date with specific symbols
  In order to get foreign exchange rate for specific date with specific symbols
  As a user with access to Rates API
  I want to receive exchange rates for specific date with specific symbols in API response

  Background:
    Given Rates api url is "https://api.ratesapi.io/api/"

  @SpecificDate @Regression
  Scenario: Martin wants to receive rate for specific date with specific symbols
      Given parameter "symbols" is "USD"
      When Martin sends a GET request for "2018-05-03" endpoint
      Then he receives response with status 200
      And he receives base rate in response as "EUR"
      And he receives date "2018-05-03" in response
      And he should see list of currencies with rates:
      | USD | 1.1992 |

  @SpecificDate @Regression
  Scenario: Martin wants to receive rate for specific date with more than one specific symbols
    Given parameter "symbols" is "THB,HKD"
    When Martin sends a GET request for "2016-07-08" endpoint
    Then he receives response with status 200
    And he receives base rate in response as "EUR"
    And he receives date "2016-07-08" in response
    And he should see list of currencies with rates:
      | THB | 39.039 |
      | HKD | 8.5866 |

  @SpecificDate @Regression
  Scenario: Martin wants to receive only one rate for specific date with two the same symbols
    And parameter "symbols" is "USD,USD"
    When Martin sends a GET request for "2017-05-05" endpoint
    Then he receives date "2017-05-05" in response
    And he should see list of currencies with rates:
      | USD | 1.0961 |
